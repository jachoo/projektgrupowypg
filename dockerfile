FROM tensorflow/tensorflow:2.0.0-py3
MAINTAINER "Sumerin"

#VARIABLES
ENV 	USER_LOGIN="pgpg" \
	USER_PASSWORD="passpg" \
	MASQUARADE_IP="172.20.83.154"

#Expose port
EXPOSE 	8000 \
	21 \
	50000-50010

#Create user account
RUN useradd -ms /bin/bash $USER_LOGIN 
RUN echo $USER_LOGIN:$USER_PASSWORD | chpasswd

#Auto answers
RUN echo 'proftpd shared/proftpd/inetd_or_standalone select standalone' | debconf-set-selections

#Packages required
RUN apt-get update && apt-get install -y\
	git \
	proftpd \
	poppler-utils \
	libglib2.0-0 \
	ffmpeg \ 
	libsm6 \ 
	libxext6 \ 
	libxrender-dev
	
RUN apt update && apt install libgl1-mesa-glx

RUN mkdir -p /projektgrupowypg
COPY ./configure-django.sh /projektgrupowypg/configure-django.sh
COPY ./docker-entrypoint.sh /projektgrupowypg/docker-entrypoint.sh
COPY ./EgzaminyPG /projektgrupowypg/EgzaminyPG
COPY ./manage.py /projektgrupowypg/manage.py
COPY ./requirements.txt /projektgrupowypg/requirements.txt
COPY ./KerasModels /projektgrupowypg/KerasModels
COPY ./Resources /projektgrupowypg/Resources
COPY ./Ftp /projektgrupowypg/Ftp
COPY ./Src /projektgrupowypg/Src
COPY ./main /projektgrupowypg/main
COPY ./Ftp/proftpd.conf /etc/proftpd/proftpd.conf

WORKDIR /projektgrupowypg

RUN chmod +x \
	./configure-django.sh \
	./docker-entrypoint.sh \
	./manage.py \
	-R ./Src/*

#Packages Python
RUN pip install --upgrade pip
RUN pip install --use-feature=2020-resolver --timeout 3600 -r requirements.txt

RUN mkdir \
	/log \
	/data


RUN ./configure-django.sh

#Run 
ENTRYPOINT ["./docker-entrypoint.sh"]


