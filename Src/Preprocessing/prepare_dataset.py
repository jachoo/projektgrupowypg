import cv2
import os
import numpy as np
from pdf2image import convert_from_path

from Src import ImgNormalization
from Src.Preprocessing.data_extractor import get_index_column, get_answer_row_raw, extract_group_row
from Src.Preprocessing.data_extractor import get_answer_row
from Src.Preprocessing.rotation import display_image


def create_dataset_columns(filename, img, directory, required_height=290, required_width=60):
    if not os.path.isdir(directory):
        print("Directory does not exist, exiting")
        exit()

    generator = get_index_column(img, required_height, required_width)

    if generator is None:
        return False

    # skip extension from filename
    filename = filename.rsplit('.', 1)[0]

    for i in range(0, 6):
        img_row = generator.__next__()

        cv2.imwrite(directory + filename + "_" + str(i + 1) + ".jpg", img_row)

    return True


def create_dataset_rows(filename, img, directory, answers_number=50, required_height=290, required_width=60):
    if not os.path.isdir(directory):
        print("Directory does not exist, exiting")
        exit()

    generator = get_answer_row_raw(img, required_height, required_width)
    group = extract_group_row(img, required_height, required_width)
    display_image("Kek", group)

    if generator is None:
        return False

    # skip extension from filename
    filename = filename.rsplit('.', 1)[0]

    for i in range(0, answers_number):
        img_row = generator.__next__()
        # cv2.imwrite(directory + filename + "_" + str(counter) + ".jpg", img_row)
        cv2.imwrite(directory + filename + "_" + str(i + 1) + ".jpg", img_row)

    return True


def get_columns_dataset(images, labels, image_shape, num_classes=11):
    if not os.path.isdir(images):
        print("Directory with images does not exist, exiting")
        exit()

    if not os.path.isdir(labels):
        print("Directory with labels does not exist, exiting")
        exit()

    x = []
    y = []

    # iterate over all dataset
    for filename_image in os.listdir(images):
        name_image, _ = os.path.splitext(images + filename_image)

        # find corresponding file with label
        for filename_label in os.listdir(labels):
            name_label, _ = os.path.splitext(images + filename_label)
            if name_image == name_label:
                # load image
                image = cv2.imread(os.path.join(images, filename_image))
                image = cv2.resize(image, (image_shape[1], image_shape[0]))

                image = noisy("s&p", image)

                # convert to grayscale
                grayscaled = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

                # convert to binary image
                binary_image = cv2.adaptiveThreshold(grayscaled, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,
                                                     115, 1)

                # convert to 3 channels
                converted_grayscaled = np.zeros_like(image)
                converted_grayscaled[:, :, 0] = grayscaled
                converted_grayscaled[:, :, 1] = grayscaled
                converted_grayscaled[:, :, 2] = grayscaled

                # convert to 3 channels
                converted_binary_image = np.zeros_like(image)
                converted_binary_image[:, :, 0] = binary_image
                converted_binary_image[:, :, 1] = binary_image
                converted_binary_image[:, :, 2] = binary_image

                # append all preprocessed images
                # x.append(image)
                x.append(converted_grayscaled)
                x.append(converted_binary_image)

                # read label file
                with open(os.path.join(labels, filename_label)) as f:
                    line = f.read().splitlines()[0]
                    label = int(line)

                # convert class to one-hot vector
                y_tmp = np.zeros(num_classes)
                np.put(y_tmp, label, 1)

                # append 3 labels for preprocessed images
                for j in range(0, 2):
                    y.append(y_tmp)

    x = np.asarray(x)
    y = np.asarray(y)

    return x, y


def get_rows_dataset(images, labels, image_shape, num_classes=4):
    if not os.path.isdir(images):
        print("Directory with images does not exist, exiting")
        exit()

    if not os.path.isdir(labels):
        print("Directory with labels does not exist, exiting")
        exit()

    X = []
    y = []

    # iterate over all dataset
    # assumes label has same name with different extension and there are exactly as many images as labels
    for filename_image, filename_label in zip(sorted(os.listdir(images)), sorted(os.listdir(labels))):
        name_image, _ = os.path.splitext(images + filename_image)
        name_label, _ = os.path.splitext(images + filename_label)
        assert name_image == name_label
        # find corresponding file with label
        # load image
        image = cv2.imread(os.path.join(images, filename_image))
        image = cv2.resize(image, (image_shape[1], image_shape[0]))

        image = noisy("s&p", image)

        # convert to grayscale
        grayscaled = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        # convert to binary image
        binary_image = cv2.adaptiveThreshold(grayscaled, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,
                                             115, 1)

        # convert to 3 channels
        converted_grayscaled = np.zeros_like(image)
        converted_grayscaled[:, :, 0] = grayscaled
        converted_grayscaled[:, :, 1] = grayscaled
        converted_grayscaled[:, :, 2] = grayscaled

        # convert to 3 channels
        converted_binary_image = np.zeros_like(image)
        converted_binary_image[:, :, 0] = binary_image
        converted_binary_image[:, :, 1] = binary_image
        converted_binary_image[:, :, 2] = binary_image

        # append all preproessed images
        # X.append(image)
        X.append(converted_grayscaled)
        X.append(converted_binary_image)

        # read label file
        with open(os.path.join(labels, filename_label)) as f:
            line = f.read().splitlines()[0]

        # convert class to one-hot vector
        y_tmp = np.zeros((num_classes))
        for i in range(0, num_classes):
            if int(line[i]) != 1 and int(line[i]) != 0:
                print(line[i] + " " + str(i) + " " + name_label)
                exit()
            np.put(y_tmp, i, int(line[i]))

        # append 3 labels for preprocessed images
        for j in range(0, 2):
            y.append(y_tmp)

    X = np.asarray(X)
    y = np.asarray(y)

    return X, y


def get_answer(index):
    # answers 1-50, indexes 0-49
    index -= 1
    answers = ["1000", "0100", "0010", "0001", "1100",  # 1-5
               "1010", "1001", "0110", "0101", "0011",  # 6-10
               "1110", "1101", "1011", "0111", "1111",  # 11-15
               "0100", "0010", "0001", "1000", "0010",  # 16-20
               "0010", "0100", "1000", "0001", "1000",  # 21-25
               "0100", "0010", "1000", "0100", "0010",  # 26-30
               "0001", "1100", "1010", "1001", "0110",  # 31-35
               "0101", "0011", "1110", "1101", "1011",  # 36-40
               "0111", "1111", "0100", "0010", "0001",  # 41-45
               "1000", "0010", "0010", "0100", "1000"]  # 46-50
    return answers[index]


def check_answer_array(index):
    answer = get_answer(index)
    letters = ['A', 'B', 'C', 'D']
    for i in range(0, 4):
        if answer[i] == '0':
            print(' ', end='')
        else:
            print(letters[i], end='')
    print('')


def get_answer_string(string):
    letters = ['A', 'B', 'C', 'D']
    out_str = letters
    for i in range(0, 4):
        if string[i] == '0':
            out_str[i] = ' '
    return ''.join(out_str)


def noisy(noise_typ, image):
    if noise_typ == "gauss":
        row, col, ch = image.shape
        mean = 0
        var = 0.1
        sigma = var ** 0.5
        gauss = np.random.normal(mean, sigma, (row, col, ch))
        gauss = gauss.reshape(row, col, ch)
        noisy = image + gauss
        return cv2.convertScaleAbs(noisy)
    elif noise_typ == "s&p":
        row, col, ch = image.shape
        s_vs_p = 0.5
        amount = 0.004
        out = np.copy(image)
        # Salt mode
        num_salt = np.ceil(amount * image.size * s_vs_p)
        coords = [np.random.randint(0, i - 1, int(num_salt))
                  for i in image.shape]
        out[coords] = 1

        # Pepper mode
        num_pepper = np.ceil(amount * image.size * (1. - s_vs_p))
        coords = [np.random.randint(0, i - 1, int(num_pepper))
                  for i in image.shape]
        out[coords] = 0
        return cv2.convertScaleAbs(out)
    elif noise_typ == "poisson":
        vals = len(np.unique(image))
        vals = 2 ** np.ceil(np.log2(vals))
        noisy = np.random.poisson(image * vals) / float(vals)
        return cv2.convertScaleAbs(noisy)
    elif noise_typ == "speckle":
        row, col, ch = image.shape
        gauss = np.random.randn(row, col, ch)
        gauss = gauss.reshape(row, col, ch)
        noisy = image + image * gauss
        return cv2.convertScaleAbs(noisy)


if __name__ == "__main__":
    directory = "../test_dataset/"
    if not os.path.isdir(directory):
        print("Directory with images does not exist, exiting")
        exit()

    X = []
    y = []

    # iterate over all dataset
    for pdf in os.listdir(directory):
        path_to_file = os.path.join(directory, pdf)
        name, ext = os.path.splitext(pdf)
        if not ext == ".pdf":
            continue
        pages = convert_from_path(path_to_file)
        for page in pages:
            page.save(name + '.jpg', 'JPEG')
            img = cv2.imread(name + '.jpg')
            if img is not None:
                img = ImgNormalization.normalize_image(img)
                rows = create_dataset_rows(name, img, directory, answers_number=50, required_height=60,
                                           required_width=290)
