import cv2
import numpy as np
from Src.Data.AnswerContainer import AnswerContainer
from Src.Preprocessing.rotation import display_image

def darken_selections(img):
    maxIntensity = 255.0
    phi = 1
    theta = 1
    img = (maxIntensity / phi) * (img / (maxIntensity / theta)) ** 2
    img = np.array(img, dtype=np.uint8)
    return img

def generate_empty_answers(count):
    result = []
    for i in range(1, count + 1):
        container = AnswerContainer()
        container.answer_id = i
        result.append(container)
    return result


def extract_index(img, debug=False, color=(0, 0, 255)):  # TODO MS: debug and color not used
    width = img.shape[1]
    height = img.shape[0]

    return img[int(2 * (height / 10)):int(25 * (height / 40)), int(12 * (width / 20)):int(19 * (width / 20))]


def extract_index_columns(img, debug=False, color=(0, 0, 255)):  # TODO MS: debug and color not used
    width = img.shape[1]
    height = img.shape[0]

    return img[int(5 * (height / 20)):int(77 * (height / 80)):, int(5 * (width / 40)):int(76 * (width / 80))]


def extract_answers_rows_coordinates(img, number, epsilon=10, debug=False,
                                     color=(0, 0, 255)):  # TODO MS: debug and color not used
    width = img.shape[1]
    height = img.shape[0]
    width_scale = width / 160
    height_scale = height / 160

    y1, y2, x1, x2 = int(51 * height_scale), int(148 * height_scale), int(14 * width_scale), int(142 * width_scale)

    if number == 1:
        return y1, y1 + int(48 * height_scale) + epsilon, x1, x1 + int(34 * width_scale)
    elif number == 2:
        return y1 + int(49 * height_scale), y2, x1, x1 + int(34 * width_scale)
    elif number == 3:
        return y1, y1 + int(48 * height_scale) + epsilon, x1 + int(48 * width_scale), x1 + int(80 * width_scale)
    elif number == 4:
        return y1 + int(49 * height_scale), y2, x1 + int(48 * width_scale), x1 + int(80 * width_scale)
    elif number == 5:
        return y1 + int(49 * height_scale), y2, x1 + int(95 * width_scale), x2

def extract_group_row(img, required_height=290, required_width=60, epsilon=10):
    width = img.shape[1]
    height = img.shape[0]

    y1, x1 = int(34 * (height / 160)), int(24 * (width / 80))

    y2, x2 = y1 + int(44 * (height / 160)) + epsilon, x1 + int(17 * (width / 80))

    height = y2 - y1
    y_start = int(height / 10) - epsilon
    y_start = max(0, y_start)

    img_row = img[y1 + y_start: y1 + int((1 + 1) * (height / 10) + epsilon), x1:x2]
    # resize to fit required shape
    x = cv2.resize(img_row, (required_height, required_width))

    x = cv2.cvtColor(x, cv2.COLOR_RGB2GRAY)
    x = cv2.cvtColor(x, cv2.COLOR_GRAY2RGB)
    x = darken_selections(x)

    x = np.reshape(x, (1, x.shape[0], x.shape[1], x.shape[2]))

    return x


def get_index_column(img, required_height=290, required_width=60):
    # extract index tableau coordinates
    index_img = extract_index(img)

    extracted_index_img = extract_index_columns(index_img)

    # width is used to calculate relative size of each column
    width = extracted_index_img.shape[1]

    for i in range(0, 6):
        # extract each column
        img_row = extracted_index_img[:, i * int(width / 6):(i + 1) * int(width / 6)]

        # resize to fit required shape
        x = cv2.resize(img_row, (required_width, required_height))
        x = cv2.cvtColor(x, cv2.COLOR_RGB2GRAY)
        x = cv2.cvtColor(x, cv2.COLOR_GRAY2RGB)
        x = darken_selections(x)
        x = np.reshape(x, (1, x.shape[0], x.shape[1], x.shape[2]))
        yield x


def get_answer_row(img, required_height=60, required_width=290):
    # create copy of original image
    img2 = img.copy()

    # overlapping area between extracted rows
    epsilon = 10

    for i in range(0, 5):
        y1, y2, x1, x2 = extract_answers_rows_coordinates(img2.copy(), i + 1)

        max_size = 10

        for j in range(0, max_size):
            height = y2 - y1
            y_start = int(j * (height / max_size)) - epsilon
            y_start = max(0, y_start)

            img_row = img[y1 + y_start: y1 + int((j + 1) * (height / max_size) + epsilon), x1:x2]
            # resize to fit required shape
            x = cv2.resize(img_row, (required_width, required_height))

            x = cv2.cvtColor(x, cv2.COLOR_RGB2GRAY)
            x = cv2.cvtColor(x, cv2.COLOR_GRAY2RGB)
            x = darken_selections(x)

            x = np.reshape(x, (1, x.shape[0], x.shape[1], x.shape[2]))
            # create answer object
            answer_container = AnswerContainer()

            answer_container.answer_id = (i * 10) + j + 1
            answer_container.x1 = x1
            answer_container.y1 = y1 + y_start
            answer_container.x2 = x2
            answer_container.y2 = y1 + int((j + 1) * (height / max_size) + epsilon)

            yield answer_container, x

def get_answer_row_raw(img, required_height=60, required_width=290):
    # create copy of original image
    img2 = img.copy()

    # overlapping area between extracted rows
    epsilon = 10

    for i in range(0, 5):
        y1, y2, x1, x2 = extract_answers_rows_coordinates(img2.copy(), i + 1)

        max_size = 10

        for j in range(0, max_size):
            height = y2 - y1
            y_start = int(j * (height / max_size)) - epsilon
            y_start = max(0, y_start)

            img_row = img[y1 + y_start: y1 + int((j + 1) * (height / max_size) + epsilon), x1:x2]
            # resize to fit required shape
            x = cv2.resize(img_row, (required_width, required_height))

            x = cv2.cvtColor(x, cv2.COLOR_RGB2GRAY)
            x = cv2.cvtColor(x, cv2.COLOR_GRAY2RGB)
            yield x
