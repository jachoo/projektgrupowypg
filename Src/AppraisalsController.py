from Src.AppraisalsFactory import get_appraisal

MODULE_NAME = "AppraisalController"



def appraise_test(test, package):
    reference_tests = [x for x in package.reference_tests if x.test_group == test.test_group]
    if len(reference_tests) == 0:
        test.acquired_points = 0
        for image in test.images:
            for answer in image.answers:
                answer.acquired_points = 0
            image.acquired_points = 0
    else:
        reference_test = reference_tests[0]
        whole_test_points = 0
        appraisal = get_appraisal(package.score_mode)
        for (image, reference_image) in zip(test.images, reference_test.images):
            image_points = 0
            for (answer, reference_answer) in zip(image.answers, reference_image.answers):
                assert answer.answer_id == reference_answer.answer_id

                actual = answer.evaluated_value
                expected = reference_answer.evaluated_value
                answer.acquired_points = appraisal(expected, actual, package.allow_negative)
                image_points += answer.acquired_points

            image.acquired_points = image_points
            whole_test_points += image_points
        test.acquired_points = whole_test_points
