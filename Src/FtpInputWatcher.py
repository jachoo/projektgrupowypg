#!/usr/bin/env python
import logging
import os

import inotify.adapters

from Src.TestEvaluator import create_empty_package
from .MainController import process_pdf_package

MODULE_NAME = 'FtpInputWatcher'
logger = logging.getLogger('exams')


def _main():
    path = "/home/{}".format(os.environ['USER_LOGIN'])#str(pathlib.Path.home())
    logger.info("starting watching over {}".format(path))
    i = inotify.adapters.Inotify()

    i.add_watch(path)

    for event in i.event_gen(yield_nones=False):
        (_, type_names, path, filename) = event

        logger.debug("PATH=[{}] FILENAME=[{}] EVENT_TYPES={}".format(path, filename, type_names))

        if type_names[0] == 'IN_CLOSE_WRITE':
            src = os.path.join(path, filename)
            package = create_empty_package(src)
            package.score_mode = 'basic'
            logger.info('{} Uploaded'.format(filename))
            process_pdf_package(filename, src, package)


if __name__ == '__main__':
    _main()
