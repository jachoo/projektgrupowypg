from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render, redirect
from django.http import HttpRequest
from django.contrib import messages
from main.forms import UserRegisterForm, TokenCreateForm
from django.http import JsonResponse, HttpResponseForbidden, HttpResponseNotFound, HttpResponse
from PIL import Image
from main.models import ImageContainer, AnswerContainer, AccessToken

from datetime import datetime
import pytz

# default template for these classes are: app/modelname_method.html
from django.views.generic import ListView, CreateView
# Use it in templates to chcek normal permissions
# {% if perms.main.view_examtemplates %}

# Create your views here.


def home(request):
    """Renders the home page."""
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/index.html',
        {
            'title':'Strona główna'
        }
    )


def register(request):
    """Renders the home page."""
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, 'Stworzono użytkownika o nazwie ' + username)
            return redirect('main-login')
    else:
        form = UserRegisterForm()
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'app/register.html',
        {'form': form}
    )


class manage_tokens(LoginRequiredMixin, ListView):
    model = AccessToken
    context_object_name = 'tokens'  # name of collection seen in template
    paginate_by = 10
    ordering = ['-expires']


class token_create(LoginRequiredMixin, CreateView):
    model = AccessToken
    success_url = '/tokenmanage/'
    form_class = TokenCreateForm


def token_expire(request, id):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    now = pytz.UTC.localize(datetime.now())
    AccessToken.objects.filter(pk=id).update(expires=now)
    return redirect('main-manage-tokens')


def test_images(request, id):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    response = {}
    for image in ImageContainer.objects.filter(test=id):
        im = Image.open(image.img_path)
        width, height = im.size
        answers = {}
        for answer in AnswerContainer.objects.filter(img=image):
            answers[answer.id] = {
                "x1": answer.x1,
                "x2": answer.x2,
                "y1": answer.y1,
                "y2": answer.y2,
                "value": answer.evaluated_value
            }
        response[image.id] = {"width": width, "height": height, "answers": answers}

    return JsonResponse(response)


def img(request, id):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    try:
        img_container = ImageContainer.objects.filter(pk=id)[0]
        with open(img_container.img_path, "rb") as f:
            return HttpResponse(f.read(), content_type="image/jpeg")
    except IOError:
        return HttpResponseNotFound()
    return


def static(request, id):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    try:
        fpath = 'main/static/{}'.format(id)
        with open(fpath, "rb") as f:
            return HttpResponse(f.read(), content_type="image/jpeg")
    except IOError:
        return HttpResponseNotFound()
    return
