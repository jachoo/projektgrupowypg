from django import forms
from datetime import datetime
from tempus_dominus.widgets import DateTimePicker
from .models import User, Package, Test, AccessToken
from django.contrib.auth.forms import UserCreationForm

import pytz


class TokenCreateForm(forms.ModelForm):
    expires = forms.DateTimeField(
        widget=DateTimePicker(
            options={
                'useCurrent': True,
                'collapse': True,
            },
            attrs={
                'append': 'fa fa-calendar',
                'icon_toggle': True,
                'size': 'small,'
            }
        )
    )

    class Meta:
        model = AccessToken
        fields = ['token', 'expires']
        widgets = {
            'token': forms.Textarea(attrs={'rows': 1, 'cols': 15}),
        }


class UserRegisterForm(UserCreationForm):
    error = {
        'token_mismatch': ("Invalid token"),
    }

    def clean_register_token(self):
        register_token = self.cleaned_data.get("register_token")
        tokens = AccessToken.objects.filter(token=register_token).iterator()
        now = pytz.UTC.localize(datetime.now())
        for token in tokens:
            if now < token.expires:
                return register_token

        raise forms.ValidationError(
            self.error['token_mismatch'],
            code='token_mismatch',
        )

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'register_token']
        widgets = {
            'register_token': forms.TextInput(),
        }


class PackageCreateForm(forms.ModelForm):

    pdf = forms.FileField(required=True)

    def __init__(self, *args, **kwargs):
        super(PackageCreateForm, self).__init__(*args, **kwargs)
        # pdf = forms.FileField(required=True)
        self.fields['pdf'].label = "Choose pdf file which contains all tests"

    class Meta(object):
        model = Package
        fields = ['name', 'pdf', 'score_mode', 'allow_negative']
        widgets = {
            'name': forms.Textarea(attrs={'rows': 1, 'cols': 15}),
        }
        labels = {
            'name': "Name of the exam",
            'pdf': "Choose pdf file which contains all tests",
            'allow_negative': "Allow negative points for answers"
        }


class PackageUpdateForm(forms.ModelForm):
    class Meta(object):
        model = Package
        fields = ['name', 'user', 'score_mode', 'allow_negative']
        widgets = {
            'name': forms.Textarea(attrs={'rows': 1, 'cols': 15}),
        }


class TestCreateForm(forms.ModelForm):
    pdf = forms.FileField(required=True)

    class Meta(object):
        model = Test
        fields = ['pdf']

class TestUpdateForm(forms.ModelForm):
    class Meta(object):
        model = Test
        fields = ['student_index', 'test_group', 'is_reference']
        widgets = {
            'student_index': forms.Textarea(attrs={'rows': 1, 'cols': 15})
        }
