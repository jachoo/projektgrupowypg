import os
import shutil

from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse

from Src.Data.Package import Package as BackendPackage
from Src.Data.AnswerContainer import AnswerContainer as BackendAnswerContainer
from Src.Data.ImageContainer import ImageContainer as BackendImageContainer
from Src.Data.Test import Test as BackendTest

import pytz
from datetime import datetime


class AccessToken(models.Model):
    token = models.TextField()
    begin = models.DateTimeField(auto_now_add=True, verbose_name="begin date")
    expires = models.DateTimeField(verbose_name="expires date")

    def __str__(self):
        return self.token

    @property
    def is_active(self):
        now = pytz.UTC.localize(datetime.now())
        return now < self.expires

    class Meta:
        verbose_name = "Token"
        verbose_name_plural = "Tokens"


class User(AbstractUser):
    email = models.EmailField(default="NotUsed@wp.pl")
    register_token = models.TextField()
    access_token = models.ForeignKey(AccessToken, on_delete=models.SET_NULL, verbose_name="Access Token", null=True)
    # is_admin = models.BooleanField(default=False)
    #REQUIRED_FIELDS = ['access_key']

    def __str__(self):
        return self.username
        
    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"


class Package(models.Model):
    folder_path = models.TextField()
    name = models.TextField()
    date_created = models.DateTimeField(auto_now_add=True, verbose_name="Creation time")
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Owner", default=None, null=True, blank=True)
    status = models.CharField(
        max_length=15,
        choices=BackendPackage.STATUS_DICT,
        default='processing',
    )
    error_message = models.CharField(
        max_length=100,
        default='',
    )
    score_mode = models.CharField(
        max_length=20,
        choices=BackendPackage.SCORING_MODES,
        default='basic',
    )
    allow_negative = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('main-package-detail', kwargs={'package':self.pk})

    def save(self, *args, **kwargs):
        pass# We are using MainController to create package, Django should not be able to create it

    def delete(self, *args, **kwargs):
        if os.path.isdir(self.folder_path):
            shutil.rmtree(self.folder_path)
        super(Package, self).delete(*args, **kwargs)

    def get_ref_test(self):
        ref_test = Test.objects.get(package=self.id, is_reference=True)
        images = []
        for image in ImageContainer.objects.filter(test=ref_test).iterator():
            answers = []
            for answer in AnswerContainer.objects.filter(img=image):
                answers.append(answer)
            image.answers = answers
            images.append(image)
        ref_test.images = images
        self.reference_test = ref_test
        return ref_test

    def cast_to_backend_object(self):
        backend_package = BackendPackage()
        backend_package.appraisal_name = self.score_mode
        backend_package.score_mode = self.score_mode
        backend_package.folder_path = self.folder_path
        backend_package.Name = self.name
        backend_package.status = self.status
        backend_package.reference_tests = []
        backend_package.error_message = self.error_message
        reference_tests = Test.objects.filter(package=self.id, is_reference=True)
        for ref_test in reference_tests:
            backend_package.reference_tests.append(ref_test.cast_to_backend_object())
        backend_package.tests = []
        backend_package.id = self.id
        backend_package.allow_negative = self.allow_negative
        for test in Test.objects.filter(package=self.id, is_reference=False).iterator():
            backend_package.tests.append(test.cast_to_backend_object())
        return backend_package


class Test(models.Model):
    acquired_points = models.IntegerField()
    student_index = models.TextField()
    test_group = models.CharField(
        max_length=1,
        choices=BackendTest.GROUPS,
        default='X',
    )
    package = models.ForeignKey(Package, on_delete=models.CASCADE)
    is_reference = models.BooleanField()

    def __str__(self):
        return self.student_index + " " + self.test_group

    def save(self, *args, **kwargs):
        pass # We are using MainController to create test, Django should not be able to create it

    def cast_to_backend_object(self):
        test = BackendTest()
        test.acquired_points = self.acquired_points
        test.student_index = self.student_index
        test.group = self.test_group
        test.id = self.id
        test.test_group = self.test_group

        test.images = []
        for image in ImageContainer.objects.filter(test=self.id).iterator():
            test.images.append(image.cast_to_backend_object())
        return test


class ImageContainer(models.Model):
    acquired_points = models.IntegerField()
    img_path = models.TextField()
    test = models.ForeignKey(Test, on_delete=models.CASCADE)

    def __str__(self):
        return self.img_path

    def get_filepath(self):
        return os.path.join(os.path.basename(os.path.dirname(self.img_path)), os.path.basename(self.img_path))

    def cast_to_backend_object(self):
        image = BackendImageContainer()
        image.acquired_points = self.acquired_points
        image.img_path = self.img_path
        image.id = self.id
        image.answers = []
        for answer in AnswerContainer.objects.filter(img=self.id).iterator():
            image.answers.append(answer.cast_to_backend_object())
        return image


class AnswerContainer(models.Model):
    acquired_points = models.IntegerField()
    evaluated_value = models.TextField()
    answer_id = models.IntegerField()
    x1 = models.IntegerField()
    x2 = models.IntegerField()
    y1 = models.IntegerField()
    y2 = models.IntegerField()
    img = models.ForeignKey(ImageContainer, on_delete=models.CASCADE)

    def cast_to_backend_object(self):
        answer = BackendAnswerContainer()
        answer.acquired_points = self.acquired_points
        answer.evaluated_value = self.evaluated_value
        answer.answer_id = self.answer_id
        answer.x1 = self.x1
        answer.x2 = self.x2
        answer.y1 = self.y1
        answer.y2 = self.y2
        return answer

# Create your models here.
