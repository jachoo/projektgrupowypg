from django import template
from django.contrib.auth.models import Group

register = template.Library()
@register.filter(name='has_group')
def has_group(user, group_name):
    group = Group.objects.get(name=group_name)
    if group in user.groups.all() or user.is_superuser:
       return True
    return False

@register.simple_tag
def get_params_without_page(val):
    result = "?"
    for param, value in val.items():
        if param != "page":
            result += param + "=" + value + "&"
    return result

@register.simple_tag
def get_test_number(value, arg):
    return value + (arg - 1) * 15
